<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Helper;

use Magento\Framework\Filesystem\DriverInterface;

class Csv extends \Magento\Framework\File\Csv
{

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $file;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csv;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $filesystem;
    /**
     * @var DriverInterface
     */
    protected $driverInterface;

    public function __construct(
        \Magento\Framework\Filesystem\DriverInterface $driverInterface,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Driver\File $file
    ) {
        $this->driverInterface = $driverInterface;
        $this->filesystem = $file;
        $this->directoryList = $directoryList;
        $this->csv = $csv;

        parent::__construct($file);
    }

    /**
     * @param string $exportName
     * @param array $data
     * @return bool
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function writeToCsv($exportName, $data){

        $fileDirectoryPath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);

        if(!$this->driverInterface->isDirectory($fileDirectoryPath))
            $this->driverInterface->createDirectory($fileDirectoryPath, 0777);
        $fileName = $exportName . '.csv';
        $filePath =  $fileDirectoryPath . '/' . $fileName;

        $this->csv
            ->setEnclosure('"')
            ->setDelimiter(',')
            ->appendData($filePath, $data);

        return true;

    }

}
