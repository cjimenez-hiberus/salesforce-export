<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Model;

abstract class AbstractExport {

    protected const   VALUE_YES   =   'S';
    protected const   VALUE_NO    =   'N';

    /**
     * @var array
     */
    private $fileHeaders = [
        'PRODUCTOS' => [
            'sku', 'nombre_producto', ' tipo_producto', 'imagen', 'precio', 'precio_venta', 'disponibilidad_web', 'categoria',
            'descripcion_breve', ' descripcion_larga', 'autor', 'proveedor', 'stock'
        ],
        'CLIENTES' => [
            'id_cliente', 'email', 'nombre_cliente', 'grupo', 'dni', 'sexo', 'fecha_nacimiento', 'ciudad', 'provincia',
            'gigya_uid', 'codigo_postal', 'direccion', 'fecha_primer_pedido', 'fecha_registro'
        ],
        'PEDIDOS' => [
            'id_pedido', 'fecha', 'importe_total', 'metodo_envio', 'estado', 'direccion', 'ciudad', 'provincia', 'codigo_postal',
            'direccion_facturacion', 'ciudad_facturacion', 'provincia_facturacion', 'codigo_postal_facturacion'
        ],
        'LINEAS_PEDIDO' => [
            'sku_producto', 'id_pedido', 'cantidad', 'importe_total', 'importe_articulo', 'iva'
        ],
        'VENTA_MEJORADA' => [
            'sku', 'sku_relacionados'
        ],
        'PRODUCTOS_RELACIONADOS' => [
            'sku', 'sku_relacionados'
        ],
        'LISTA_DESEOS' => [
            'sku', 'id_cliente'
        ],
        'VENTA_CRUZADA' => [
            'sku', 'sku_relacionados'
        ]
    ];

    /**
     * @return array
     *
     * Process Main Function
     */
    abstract function export();

    /**
     * @param string $file
     * @return array
     */
    protected function getFileHeaders($file)
    {
        return (isset($this->fileHeaders[$file])) ? $this->fileHeaders[$file] : [];
    }

}
