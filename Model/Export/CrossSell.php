<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Model\Export;

class CrossSell extends \Hiberus\Salesforce\Model\AbstractExport {

    private const IDENTITY_NAME = 'VENTA_CRUZADA';

    /**
     * @var \Hiberus\Salesforce\Model\Products
     */
    protected $products;

    /**
     * ProductExport constructor.
     * @param \Hiberus\Salesforce\Model\Products $products
     */
    public function __construct(
        \Hiberus\Salesforce\Model\Products $products
    ) {
        $this->products = $products;
    }

    /**
     * @return array
     */
    public function export() {

        $data = [
            'fileName' => self::IDENTITY_NAME,
            'data' => ''
        ];

        $data['data'] = $this->getFileHeaders(self::IDENTITY_NAME);

        $products = $this->products->getProducts();

        if ($products != null) {

            $crossSellProducts =
                \array_filter(
                    \array_map([$this, 'getCrossSellProducts'], $products)
                );

            \array_push($data['data'], $crossSellProducts);

        }

        return $data;

    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getCrossSellProducts($product) {

        if ($this->getCrossSellProductsSku($product) != null) {
            return [
                'sku' => $product->getSku(),
                'sku_relacionados' => \implode(',', $this->getCrossSellProductsSku($product))
            ];
        }

        return [];

    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array|bool
     */
    protected function getCrossSellProductsSku($product) {

        $skuList = [];

        $crossSellProducts = $product->getCrossSellProducts();

        if ($crossSellProducts != null) {

            foreach ($crossSellProducts as $crossSellProduct) {
                $skuList[] = $crossSellProduct->getSku();
            }

            return $skuList;

        }

        return $skuList;

    }

}
