<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Model\Export;

use Magento\Framework\Api\SearchCriteriaBuilder;

class Order extends \Hiberus\Salesforce\Model\AbstractExport {

    private const IDENTITY_NAME = 'PEDIDOS';

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * ProductExport constructor.
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return array
     */
    public function export() {

        $data = [
            'fileName' => self::IDENTITY_NAME,
            'data' => ''
        ];

        $orderList = [];
        $orderList[] = $this->getFileHeaders(self::IDENTITY_NAME);

        $orders = $this->orderRepository->getList($this->searchCriteriaBuilder->create())->getItems();

        if ($orders != null) {

            /** @var \Magento\Sales\Model\Order $order */
            foreach ($orders as $order) {

                $billingAddress = $order->getBillingAddress();
                $shippingAddress = $order->getShippingAddress();

                $orderList[] = [
                    'id_pedido' => $order->getId(),
                    'fecha_pedido' => $order->getCreatedAt(),
                    'importe_total' => $order->getGrandTotal(),
                    'metodo_envio' => $order->getShippingDescription(),
                    'estado' => $order->getStatus(),
                    'direccion' => $shippingAddress->getStreet()[0],
                    'ciudad' => $shippingAddress->getCity(),
                    'provincia' => $shippingAddress->getRegionCode(),
                    'codigo_postal' => $shippingAddress->getPostcode(),
                    'direccion_facturacion' => $billingAddress->getStreet()[0],
                    'ciudad_facturacion' => $billingAddress->getCity(),
                    'provincia_facturacion' => $billingAddress->getRegionCode(),
                    'codigo_postal_facturacion' => $billingAddress->getPostcode()
                ];
            }

            $data['data'] = $orderList;

        }

        return $data;

    }

}
