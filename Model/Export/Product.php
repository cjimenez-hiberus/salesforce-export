<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Model\Export;

use Magento\Framework\Api\SearchCriteriaBuilder;

class Product extends \Hiberus\Salesforce\Model\AbstractExport {

    private const IDENTITY_NAME = 'PRODUCTOS';

    /**
     * @var \Hiberus\Salesforce\Model\Products
     */
    protected $products;

    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    protected $stock;

    /**
     * ProductExport constructor.
     * @param \Hiberus\Salesforce\Model\Products $products
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository $stock
     */
    public function __construct(
        \Hiberus\Salesforce\Model\Products $products,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stock
    ) {
        $this->products = $products;
        $this->categoryRepository = $categoryRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->stock = $stock;
        }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function export() {

        $data = [
            'fileName' => self::IDENTITY_NAME,
            'data' => ''
        ];

        $productList = [];
        $productList[] = $this->getFileHeaders(self::IDENTITY_NAME);

        $products = $this->products->getProducts();

        if ($products != null) {

            /** @var \Magento\Catalog\Model\Product $product */
            foreach ($products as $product) {

                $productId = $product->getId();

                $categories = \implode(
                    ',',
                    $this->getCategories(
                        $product->getCategoryIds()
                    )
                );

                $description = (string) $product->getData('description');

                $productList[] = [
                    'sku' => $product->getSku(),
                    'nombre' => $product->getName(),
                    'tipo' => $product->getTypeId(),
                    'imagen' => $product->getData('image'),
                    'precio' => $product->getPrice(),
                    'precio_venta' => $product->getData('msrp_display_actual_price_type'), //confirmar dato
                    'disponible_web' => $product->getStatus() == 1 ? self::VALUE_YES : self::VALUE_NO,
                    'categorias' => $categories,
                    'descripcion_corta' => $product->getData('short_description'),
                    'descripcion' => \strip_tags($description),
                    'autor' => '', //$product->getCustomAttribute('author'); necesito ejemplo BD
                    'proveedor' => '', //$product->getCustomAttribute('supplier'); necesito ejemplo BD
                    'stock' => $this->stock->get($productId)->getQty() //mas limpio dentro de una variable?
                ];
            }

            $data['data'] = $productList;

        }

        return $data;

    }

    /**
     * @param array $categoryIds
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCategories($categoryIds) {

        $this->searchCriteriaBuilder->addFilter(
            'id',
            $categoryIds,
            'in'
        );

        $categories = [];
        if ($categoryIds != null) {
            foreach ($categoryIds as $category) {
                $categoryData = $this->categoryRepository->get($category);
                $categories[] = $categoryData->getName();
            }
        }

        return $categories;

    }

}
