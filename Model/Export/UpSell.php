<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Model\Export;

class UpSell extends \Hiberus\Salesforce\Model\AbstractExport {

    private const IDENTITY_NAME = 'VENTA_MEJORADA';

    /**
     * @var \Hiberus\Salesforce\Model\Products
     */
    protected $products;

    /**
     * ProductExport constructor.
     * @param \Hiberus\Salesforce\Model\Products $products
     */
    public function __construct(
        \Hiberus\Salesforce\Model\Products $products
    ) {
        $this->products = $products;
    }

    /**
     * @return array
     */
    public function export() {

        $data = [
            'fileName' => self::IDENTITY_NAME,
            'data' => ''
        ];

        $data['data'] = $this->getFileHeaders(self::IDENTITY_NAME);

        $products = $this->products->getProducts();

        if ($products != null) {

            $upSellProducts =
                \array_filter(
                    \array_map([$this, 'getUpSellProducts'], $products)
                );

            \array_push($data['data'], $upSellProducts);

        }

        return $data;

    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getUpSellProducts($product) {

        if ($this->getUpSellProductsSku($product) != null) {
            return [
                'sku' => $product->getSku(),
                'sku_relacionados' => \implode(',', $this->getUpSellProductsSku($product))
            ];
        }

        return [];

    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array|bool
     */
    protected function getUpSellProductsSku($product) {

        $skuList = [];

        $upSellProducts = $product->getUpSellProducts();

        if ($upSellProducts != null) {

            foreach ($upSellProducts as $upSellProduct) {
                $skuList[] = $upSellProduct->getSku();
            }

            return $skuList;

        }

        return $skuList;

    }

}
