<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Model\Export;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Exception\NoSuchEntityException;

class Customer extends \Hiberus\Salesforce\Model\AbstractExport {

    private const IDENTITY_NAME = 'CLIENTES';

    /**
     * @var int|void
     */
    protected $total;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $customerAddress;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection
     */
    protected $attributeOptionCollection;

    /**
     * @var \Magento\Eav\Api\AttributeRepositoryInterface
     */
    protected $eavAttributeRepositoryInterface;

    /**
     * @var \Magento\Customer\Api\GroupRepositoryInterface
     */
    protected $groupRepository;

    /**
     * CustomerExport constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $attributeOptionCollection
     * @param \Magento\Eav\Api\AttributeRepositoryInterface $eavAttributeRepositoryInterface
     * @param \Magento\Customer\Api\GroupRepositoryInterface $groupRepository
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $attributeOptionCollection,
        \Magento\Eav\Api\AttributeRepositoryInterface $eavAttributeRepositoryInterface,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->customerAddress = $addressRepository;
        $this->orderRepository = $orderRepository;
        $this->attributeOptionCollection = $attributeOptionCollection;
        $this->eavAttributeRepositoryInterface = $eavAttributeRepositoryInterface;
        $this->groupRepository = $groupRepository;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function export() {

        $data = [
            'fileName' => self::IDENTITY_NAME,
            'data' => ''
        ];

        $customerList = [];
        $customerList[] = $this->getFileHeaders(self::IDENTITY_NAME);

        $customers = $this->customerRepository
            ->getList($this->searchCriteriaBuilder->create())
            ->getItems();

        if ($customers != null) {

            /** @var \Magento\Customer\Model\Customer $customer */
            foreach ($customers as $customer) {

                //find keys or associated keys
                $customerId = $customer->getId();

                $customerAddress = $customer->getDefaultShipping();
                if ($customerAddress != 0) {
                    $address = $this->customerAddress->getById($customerAddress);
                    $city = $address->getCity();
                    $region = $address->getRegion()->getRegion();
                    $postcode = $address->getPostcode();
                    $street = $address->getStreet()[0];
                }

                $getFirstOrder = $this->getCustomerFirstOrder($customerId);
                $firstOrder = '';
                if ($getFirstOrder == true) {
                    $firstOrder = $getFirstOrder->getCreatedAt();
                }

                $customerName = '';
                $customerName .= $customer->getFirstname();
                $customerName .= ' ' . $customer->getMiddlename();
                $customerName .= ' ' . $customer->getLastname();

                $group = $this->getCustomerGroup($customer->getGroupId());
                $gender = $this->getCustomerGender($customer->getGender());

                $customerList[] = [
                    'id_cliente' => $customerId,
                    'email' => $customer->getEmail(),
                    'nombre_cliente' => $customerName,
                    'grupo' => $group,
                    'dni' => '', //$customer->getCustomAttribute('dni'), confirmar
                    'sexo' => $gender,
                    'fecha_nacimiento' => $customer->getDob(),
                    'ciudad' => $city ?? '',
                    'provincia' => $region ?? '',
                    'gigya_uid' => '', //$customer->getCustomAttribute('gigya_uid'), confirmar
                    'codigo_postal' => $postcode ?? '',
                    'direccion' => $street ?? '',
                    'fecha_primer_pedido' => $firstOrder,
                    'fecha_registro' => $customer->getCreatedAt()
                ];

            }

            $data['data'] = $customerList;

        }

        return $data;

    }

    /**
     * @param string $customerId
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    protected function getCustomerFirstOrder($customerId) {

        $filter = [];

        $filter[] =
            $this->filterBuilder
                 ->setField('customer_id')
                 ->setConditionType('eq')
                 ->setValue($customerId)
                 ->create();

        $this->searchCriteriaBuilder->addFilters($filter);

        $orders = $this->orderRepository
                    ->getList(
                        $this->searchCriteriaBuilder
                            ->setPageSize(1)
                            ->setCurrentPage(1)
                            ->create()
                    )
                    ->getItems();

        return \current($orders);

    }

    /**
     * @param int $genderId
     * @return string
     */
    protected function getCustomerGender($genderId) {

        $attributeId = $this->eavAttributeRepositoryInterface
            ->get('customer', 'gender')
            ->getAttributeId();

        $optionData = $this->attributeOptionCollection
            ->setAttributeFilter($attributeId)
            ->setIdFilter($genderId)
            ->setStoreFilter();

        return $optionData->getData()[0]['value'] ?? '';

    }

    /**
     * @param int $groupId
     * @return string
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getCustomerGroup($groupId) {

        $group = $this->groupRepository->getById($groupId);
        return $group->getCode();

    }

}
