<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Model\Export;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Wishlist\Model\WishlistFactory;

class Wishlist extends \Hiberus\Salesforce\Model\AbstractExport {

    private const IDENTITY_NAME = 'LISTA_DESEOS';

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var WishlistFactory
     */
    protected $wishlistFactory;

    /**
     * @var \Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var array
     */
    protected $wishlistItems;

    /**
     * CustomerExport constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param WishlistFactory $wishlistFactory
     * @param \Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory $collectionFactory
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        \Magento\Wishlist\Model\WishlistFactory $wishlistFactory,
        \Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory $collectionFactory
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->wishlistFactory = $wishlistFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function export() {

        $data = [
            'fileName' => self::IDENTITY_NAME,
            'data' => ''
        ];

        $customers = $this->customerRepository
            ->getList($this->searchCriteriaBuilder->create())
            ->getItems();

        $this->wishlistItems = [];
        $this->wishlistItems[] = $this->getFileHeaders(self::IDENTITY_NAME);

        if ($customers != null) {

            foreach ($customers as $customer) {
                $customerId = $customer->getId();
                $this->getCustomerWishlist($customerId);
            }

            $data['data'] = $this->wishlistItems;

        }

        return $data;

    }

    /**
     * @param int $customerId
     * @return array
     */
    protected function getCustomerWishlist($customerId) {

        $result = [];

        $wishlist = $this->wishlistFactory
            ->create()
            ->loadByCustomerId($customerId)
            ->getItemCollection();

        foreach ($wishlist as $item) {
            $this->wishlistItems[] = [
                'sku' => $item->getProduct()->getSku(),
                'customer_id' => $customerId
            ];
        }

        return $result;

    }

}
