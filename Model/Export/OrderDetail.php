<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Model\Export;

use Magento\Framework\Api\SearchCriteriaBuilder;

class OrderDetail extends \Hiberus\Salesforce\Model\AbstractExport {

    private const IDENTITY_NAME = 'LINEAS_PEDIDO';

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Sales\Api\OrderItemRepositoryInterface
     */
    protected $orderItemRepository;

    /**
     * ProductExport constructor.
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return array
     */
    public function export() {

        $data = [
            'fileName' => self::IDENTITY_NAME,
            'data' => ''
        ];

        $orderList = [];
        $orderList[] = $this->getFileHeaders(self::IDENTITY_NAME);

        $orderDetails = $this->orderItemRepository->getList($this->searchCriteriaBuilder->create())->getItems();

        if ($orderDetails != null) {

            foreach ($orderDetails as $detail) {

                $orderList[] = [
                    'sku' => $detail->getSku(),
                    'id_pedido' => $detail->getOrderId(),
                    'cantidad' => $detail->getQtyOrdered(),
                    'importe_total' => $detail->getRowTotal(),
                    'importe_articulo' => $detail->getPrice(),
                    'iva' => $detail->getTaxAmount()
                ];

            }

            $data['data'] = $orderList;

        }

        return $data;

    }

}
