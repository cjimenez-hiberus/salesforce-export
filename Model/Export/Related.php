<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Model\Export;

class Related extends \Hiberus\Salesforce\Model\AbstractExport {

    private const IDENTITY_NAME = 'PRODUCTOS_RELACIONADOS';

    /**
     * @var \Hiberus\Salesforce\Model\Products
     */
    protected $products;

    /**
     * ProductExport constructor.
     * @param \Hiberus\Salesforce\Model\Products $products
     */
    public function __construct(
        \Hiberus\Salesforce\Model\Products $products
    ) {
        $this->products = $products;
    }

    /**
     * @return array
     */
    public function export() {

        $data = [
            'fileName' => self::IDENTITY_NAME,
            'data' => ''
        ];

        $data['data'] = $this->getFileHeaders(self::IDENTITY_NAME);

        $products = $this->products->getProducts();

        if ($products != null) {

            $relatedProducts =
                \array_filter(
                    \array_map([$this, 'getRelatedProducts'], $products)
                );

            \array_push($data['data'], $relatedProducts);

        }

        return $data;

    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getRelatedProducts($product) {

        if ($this->getRelatedProductsSku($product) != null) {
            return [
                'sku' => $product->getSku(),
                'sku_relacionados' => \implode(',', $this->getRelatedProductsSku($product))
            ];
        }

        return [];

    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array|bool
     */
    protected function getRelatedProductsSku($product) {

        $skuList = [];

        $relatedProducts = $product->getRelatedProducts();

        if ($relatedProducts != null) {

            foreach ($relatedProducts as $relatedProduct) {
                $skuList[] = $relatedProduct->getSku();
            }

            return $skuList;

        }

        return $skuList;

    }

}
