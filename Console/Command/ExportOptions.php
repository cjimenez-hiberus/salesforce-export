<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Console\Command;

class ExportOptions
{

    /**
     * Export name option
     */
    private const INPUT_KEY_PROCESSES = 'export';

    /**
     * Command options list
     *
     * @return array
     */
    public function getOptionsList()
    {
        return $this->getBasicOptions();
    }

    /**
     * Basic options
     *
     * @return array
     */
    private function getBasicOptions()
    {
        return [
            new \Symfony\Component\Console\Input\InputArgument(
                self::INPUT_KEY_PROCESSES,
                \Symfony\Component\Console\Input\InputArgument::OPTIONAL
                | \Symfony\Component\Console\Input\InputArgument::IS_ARRAY,
                'Space-separated list of exports or omit to generate all exports.'
            )
        ];
    }
}
