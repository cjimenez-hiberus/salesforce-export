<?php

/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

namespace Hiberus\Salesforce\Console\Command;

use Symfony\Component\Console\Command\Command;

class ExportCommand extends Command
{

    private const INPUT_KEY_EXPORTS = 'export';

    /**
     * @var ExportOptions
     */
    protected $exportOptions;

    /**
     * @var array
     */
    protected $exportList;

    /**
     * @var \Hiberus\Salesforce\Model\Export\Product
     */
    protected $productExport;

    /**
     * @var \Hiberus\Salesforce\Model\Export\Customer
     */
    protected $customerExport;

    /**
     * @var \Hiberus\Salesforce\Model\Export\Order
     */
    protected $orderExport;

    /**
     * @var \Hiberus\Salesforce\Model\Export\OrderDetail
     */
    protected $orderLinesExport;

    /**
     * @var \Hiberus\Salesforce\Model\Export\UpSell
     */
    protected $upSellExport;

    /**
     * @var \Hiberus\Salesforce\Model\Export\CrossSell
     */
    protected $crossSellExport;

    /**
     * @var \Hiberus\Salesforce\Model\Export\Wishlist
     */
    protected $wishlistExport;

    /**
     * @var \Hiberus\Salesforce\Model\Export\Related
     */
    protected $relatedExport;

    /**
     * @var \Hiberus\Salesforce\Helper\Csv
     */
    protected $csv;

    /**
     * @var false|string
     */
    private $date;

    /**
     * ExportCommand constructor.
     * @param ExportOptions $options
     * @param \Hiberus\Salesforce\Model\Export\Product $productExport
     * @param \Hiberus\Salesforce\Model\Export\Customer $customerExport
     * @param \Hiberus\Salesforce\Model\Export\Order $orderExport
     * @param \Hiberus\Salesforce\Model\Export\OrderDetail $orderLinesExport
     * @param \Hiberus\Salesforce\Model\Export\UpSell $upSellExport
     * @param \Hiberus\Salesforce\Model\Export\CrossSell $crossSellExport
     * @param \Hiberus\Salesforce\Model\Export\Wishlist $wishlistExport
     * @param \Hiberus\Salesforce\Model\Export\Related $relatedExport
     * @param \Hiberus\Salesforce\Helper\Csv $csv
     */
    public function __construct(
        \Hiberus\Salesforce\Console\Command\ExportOptions $options,
        \Hiberus\Salesforce\Model\Export\Product $productExport,
        \Hiberus\Salesforce\Model\Export\Customer $customerExport,
        \Hiberus\Salesforce\Model\Export\Order $orderExport,
        \Hiberus\Salesforce\Model\Export\OrderDetail $orderLinesExport,
        \Hiberus\Salesforce\Model\Export\UpSell $upSellExport,
        \Hiberus\Salesforce\Model\Export\CrossSell $crossSellExport,
        \Hiberus\Salesforce\Model\Export\Wishlist $wishlistExport,
        \Hiberus\Salesforce\Model\Export\Related $relatedExport,
        \Hiberus\Salesforce\Helper\Csv $csv
    ) {

        $this->exportOptions = $options;

        $this->productExport = $productExport;
        $this->customerExport = $customerExport;
        $this->orderExport = $orderExport;
        $this->orderLinesExport = $orderLinesExport;
        $this->upSellExport = $upSellExport;
        $this->crossSellExport = $crossSellExport;
        $this->wishlistExport = $wishlistExport;
        $this->relatedExport = $relatedExport;
        $this->csv = $csv;

        $this->date = \date('Y-m-d');

        $this->initExportList();

        parent::__construct();

    }

    protected function configure()
    {

        $this->setName('hiberus:salesforce:export')
             ->setDescription('Export data for salesforce into csv files.')
             ->setDefinition($this->exportOptions->getOptionsList());

        parent::configure();

    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {

        foreach ($this->getExports($input) as $exportEntity) {
            try {
                $export = $exportEntity->export();

                $fileName = $export['fileName'];
                $exportData = $export['data'];

                $this->csv->writeToCsv($this->date . ' ' .  $fileName, $exportData);

                $output->writeln('<info>' . $fileName . ' file exported succesfully.</info>');
            } catch (\Throwable $e) {
                $output->writeln('Unknown error:');
                $output->writeln($e->getMessage());
            }
        }

        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;

    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @return \Hiberus\Salesforce\Model\AbstractExport[]
     */
    protected function getExports(\Symfony\Component\Console\Input\InputInterface $input) {

        $requestedExports = [];
        if ($input->getArgument(self::INPUT_KEY_EXPORTS) != null) {
            $requestedExports = $input->getArgument(self::INPUT_KEY_EXPORTS);
            $requestedExports = \array_filter(\array_map('trim', $requestedExports));
        }

        if ($requestedExports == null) {
            $exports = $this->getAllExports();
        } else {
            $availableIndexers = $this->getAllExports();
            $unsupportedExports = \array_diff($requestedExports, \array_keys($availableIndexers));
            if ($unsupportedExports != null) {
                throw new \InvalidArgumentException(
                    "The following requested exports are not supported: '" . \join("', '", $unsupportedExports)
                    . "'." . PHP_EOL . 'Supported exports: ' . \join(", ", \array_keys($availableIndexers))
                );
            }
            $exports = \array_intersect_key($availableIndexers, \array_flip($requestedExports));
        }

        return $exports;

    }

    /**
     * @return array
     */
    protected function getAllExports() {
        return $this->exportList;
    }

    protected function initExportList() {
        $this->exportList = [
            'product' => $this->productExport,
            'customer' => $this->customerExport,
            'order' => $this->orderExport,
            'order_lines' => $this->orderLinesExport,
            'upsell' => $this->upSellExport,
            'cross_sell' => $this->crossSellExport,
            'wishlist' => $this->wishlistExport,
            'related' => $this->relatedExport
        ];
    }

}
