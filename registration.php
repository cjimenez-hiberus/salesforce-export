<?php
/**
 * @author: Carlos Jiménez
 */

declare(strict_types=1);

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Hiberus_Salesforce',
    __DIR__
);
